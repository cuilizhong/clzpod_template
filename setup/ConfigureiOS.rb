module Pod

  class ConfigureIOS
    attr_reader :configurator

    def self.perform(options)
      new(options).perform
    end

    def initialize(options)
      @configurator = options.fetch(:configurator)
    end

    def perform

      # configurator.add_pod_to_podfile "Specta"
      # configurator.add_pod_to_podfile "Expecta"
      #
      # configurator.add_line_to_pch "@import Specta;"
      # configurator.add_line_to_pch "@import Expecta;"
      #
      # configurator.set_test_framework("specta", "m")
      #
      #
      #
      #
      # configurator.add_pod_to_podfile "FBSnapshotTestCase"
      # configurator.add_line_to_pch "@import FBSnapshotTestCase;"
      #
      # configurator.add_pod_to_podfile "Expecta+Snapshots"
      # configurator.add_line_to_pch "@import Expecta_Snapshots;"

      # prefix = nil
      #
      # loop do
      #   prefix = configurator.ask("What is your class prefix")
      #
      #   if prefix.include?(' ')
      #     puts 'Your class prefix cannot contain spaces.'.red
      #   else
      #     break
      #   end
      # end


      if configurator.pod_type.downcase == "module"
        `rm -rf Pod/Classes/Mediators`
      elsif configurator.pod_type.downcase == "mediator"
        `rm -rf Pod/Classes/Target_Actions`
      else
        `rm -rf Pod/Classes/Target_Actions`
        `rm -rf Pod/Classes/Mediators`
      end


      if configurator.pod_type.downcase == "module" or configurator.pod_type.downcase == "mediator"
        Pod::PodContentManipulator.new({
          :configurator => @configurator,
          :pod_content_path => "Pod",
          :platform => :ios,
          :prefix => 'CLZ'
        }).run
      end


      Pod::ProjectManipulator.new({
        :configurator => @configurator,
        :xcodeproj_path => "templates/ios/Example/PROJECT.xcodeproj",
        :platform => :ios,
        :remove_demo_project => false,
        :prefix => 'CLZ'
      }).run

      `mv ./templates/ios/* ./`



      # There has to be a single file in the Classes dir
      # or a framework won't be created, which is now default
      `touch Pod/Classes/ReplaceMe.m`

    end
  end

end


module Pod

  class PodContentManipulator
    attr_reader :configurator, :pod_content_path, :platform, :string_replacements, :prefix

    def self.perform(options)
      new(options).perform
    end

    def initialize(options)
      @pod_content_path = options.fetch(:pod_content_path)
      @configurator = options.fetch(:configurator)
      @platform = options.fetch(:platform)
      @prefix = options.fetch(:prefix)
    end

    def run
      @string_replacements = {
        "PROJECT_OWNER" => @configurator.local_user_name,
        "TODAYS_DATE" => @configurator.date,
        "TODAYS_YEAR" => @configurator.year,
        "PROJECT" => @configurator.pod_sub_name
      }
      replace_pod_content_settings

      rename_files
      # rename_project_folder
    end

    def rename_files
      Dir.glob(@pod_content_path + "/**/**/**/**").each do |name|
        next if Dir.exists? name
        puts name
        File.rename name, name.gsub("PROJECT", @configurator.pod_sub_name)

      end

    end

    # def rename_project_folder
    #   if Dir.exist? project_folder + "/PROJECT"
    #     File.rename(project_folder + "/PROJECT", project_folder + "/" + @configurator.pod_name)
    #   end
    # end

    # 修改pod class内文件配置格式
    def replace_pod_content_settings
      Dir.glob(@pod_content_path + "/**/**/**/**").each do |name|
        next if Dir.exists? name
        text = File.read(name)

        for find, replace in @string_replacements
            text = text.gsub(find, replace)
        end

        File.open(name, "w") { |file| file.puts text }
      end
    end

  end

end

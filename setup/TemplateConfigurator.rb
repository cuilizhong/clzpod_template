require 'fileutils'
require 'colored2'

module Pod
  class TemplateConfigurator

    attr_reader :pod_name, :pod_sub_name, :pod_summary, :pod_types, :pod_type, :pod_project_id, :pods_for_podfile, :pods_for_podspec, :prefixes, :test_example_file, :username, :email

    def initialize(pod_name)
      @pod_name = pod_name
      @pods_for_podfile = []
      @pods_for_podspec = []
      @prefixes = []
      @pod_types = ["UI", "BASIC", "MODULE", "MEDIATOR"]
      @message_bank = MessageBank.new(self)
      @pod_summary = nil
      @pod_type = nil
      # MODULE_MFMain UI_MFxxx BASIC_MFXXX   _右边 name
      @pod_sub_name = nil
      # pod 所属项目编号，后面的branch等都用pod_project_id来命名。此参数为空时，branch=master,产品线分支
      @pod_project_id = nil
    end

    def ask(question)
      answer = ""
      loop do
        puts "\n#{question}?"

        @message_bank.show_prompt
        answer = gets.chomp

        break if answer.length > 0

        print "\nYou need to provide an answer."
      end
      answer
    end

    def ask_with_answer(question, default_answer)
      answer = ""
      loop do
        puts "\n#{question}?"

        @message_bank.show_prompt
        answer = gets.chomp

        if answer == ""
          answer = default_answer
          print answer.yellow
        end

        break if answer.length > 0

      end
      answer
    end

    def ask_with_answers(question, possible_answers)

      print "\n#{question}? ["

      print_info = Proc.new {

        possible_answers_string = possible_answers.each_with_index do |answer, i|
           _answer = (i == 0) ? answer.underlined : answer
           print " " + _answer
           print(" /") if i != possible_answers.length-1
        end
        print " ]\n"
      }
      print_info.call

      answer = ""

      loop do
        @message_bank.show_prompt
        answer = gets.chomp

        answer = "yes" if answer.downcase == "y"
        answer = "no" if answer.downcase == "n"

        # default to first answer
        if answer == ""
          answer = possible_answers[0]
          print answer.yellow
        end

        break if possible_answers.map { |a| a.downcase }.include? answer.downcase

        print "\nPossible answers are ["
        print_info.call
      end

      answer
    end

    def run
      @message_bank.welcome_message

      pod_subs = @pod_name.split('_');
      if pod_subs.size == 2 and @pod_types.include?(pod_subs[0]) and (pod_subs[1].length > 0)
        @pod_type = pod_subs[0]
        @pod_sub_name = pod_subs[1]
      else
        puts 'pod name is not format!!!'
        exit
      end

      # @pod_type = self.ask_with_answers("What type is the pod", ["UI", "BASIC", "MODULE", "MEDIATOR"])
      if @pod_type.downcase == "mediator"
        add_pod_to_podspec "MEDIATOR_YMBase"
      end

      puts "pod type #@pod_type"

      loop do
        @pod_summary = self.ask("What is your pod summary")

        if @pod_summary.empty?
          puts 'pod summary can not empty.'.red
        else
          break
        end
      end

      @pod_project_id = self.ask_with_answer("What is your pod belongs project id (项目编号，见协同)", "master")

      # if @pod_project_id.empty?
      #   puts 'your pod belongs product line.'.red
      #   @pod_project_id = 'master'
      # end

      # case framework
      #   when :ui
      #     ConfigureSwift.perform(configurator: self)
      #
      #   when :basic
      #     ConfigureIOS.perform(configurator: self)
      #
      #   when :module
      #
      # end
      ConfigureIOS.perform(configurator: self)

      replace_variables_in_files
      add_pods_to_podspec
      clean_template_files
      rename_template_files
      add_pods_to_podfile
      customise_prefix
      rename_classes_folder
      ensure_carthage_compatibility



      # loop do
      #   repo = configurator.ask("What is your pod repo?")
      #
      #   if repo.empty?
      #     puts 'pod repo can not empty.'.red
      #   else
      #     break
      #   end
      # end

      reinitialize_git_repo
      run_pod_install

      @message_bank.farewell_message
    end

    #----------------------------------------#

    def ensure_carthage_compatibility
      FileUtils.ln_s('Example/Pods/Pods.xcodeproj', '_Pods.xcodeproj')
    end

    def run_pod_install
      puts "\nRunning " + "pod install".magenta + " on your new library."
      puts ""

      Dir.chdir("Example") do
        system "pod install"
      end

      `git add Example/#{pod_name}.xcodeproj/project.pbxproj`
      `git add -A`
      `git commit -m "Initial commit"`
      # `git remote add origin http://gitlab-cm.retech.local/ios-product-line/#{pod_type.downcase}-#{pod_name.downcase}.git`
      `git remote add origin https://gitee.com/cuilizhong/#{pod_name.downcase}.git`

    end

    def clean_template_files
      ["./**/.gitkeep", "configure", "_CONFIGURE.rb", "README.md", "LICENSE", "templates", "setup", "CODE_OF_CONDUCT.md"].each do |asset|
        `rm -rf #{asset}`
      end
    end

    def replace_variables_in_files
      file_names = ['POD_LICENSE', 'POD_README.md', 'NAME.podspec', '.travis.yml', podfile_path]
      file_names.each do |file_name|
        text = File.read(file_name)
        text.gsub!("${POD_NAME}", @pod_name)
        text.gsub!("${REPO_NAME}", @pod_name.gsub('+', '-'))
        text.gsub!("${POD_NAME_DOWNCASE}", @pod_name.downcase)
        text.gsub!("${USER_NAME}", user_name)
        text.gsub!("${USER_EMAIL}", user_email)
        text.gsub!("${YEAR}", year)
        text.gsub!("${DATE}", date)
        text.gsub!("${POD_SUMMARY}", @pod_summary)
        text.gsub!("${POD_TYPE}", @pod_type.downcase)
        text.gsub!("${POD_PROJECT_ID}", @pod_project_id.downcase)
        File.open(file_name, "w") { |file| file.puts text }
      end
    end

    def add_pod_to_podfile podname
      @pods_for_podfile << podname
    end

    def add_pod_to_podspec podname
      @pods_for_podspec << podname
    end

    def add_pods_to_podfile
      podfile = File.read podfile_path
      podfile_content = @pods_for_podfile.map do |pod|
        "pod '" + pod + "'"
      end.join("\n  ")
      podfile.gsub!("${INCLUDED_PODS}", podfile_content)
      File.open(podfile_path, "w") { |file| file.puts podfile }
    end

    def add_pods_to_podspec
      podspec = File.read podspec_path
      podspec_content = @pods_for_podspec.map do |pod|
        "s.dependency '" + pod + "'"
      end.join("\n  ")
      podspec.gsub!("${INCLUDED_PODS}", podspec_content)
      File.open(podspec_path, "w") { |file| file.puts podspec }
    end

    def add_line_to_pch line
      @prefixes << line
    end

    def customise_prefix
      prefix_path = "Example/Tests/Tests-Prefix.pch"
      return unless File.exists? prefix_path

      pch = File.read prefix_path
      pch.gsub!("${INCLUDED_PREFIXES}", @prefixes.join("\n  ") )
      File.open(prefix_path, "w") { |file| file.puts pch }
    end

    def set_test_framework(test_type, extension)
      content_path = "setup/test_examples/" + test_type + "." + extension
      tests_path = "templates/" + "ios" + "/Example/Tests/Tests." + extension
      tests = File.read tests_path
      tests.gsub!("${TEST_EXAMPLE}", File.read(content_path) )
      File.open(tests_path, "w") { |file| file.puts tests }
    end

    def rename_template_files
      FileUtils.mv "POD_README.md", "README.md"
      FileUtils.mv "POD_LICENSE", "LICENSE"
      FileUtils.mv "NAME.podspec", "#{pod_name}.podspec"
    end

    def rename_classes_folder
      FileUtils.mv "Pod", @pod_name
    end

    def reinitialize_git_repo
      `rm -rf .git`
      `git init`
      `git checkout -b #{pod_project_id}`
      `git add -A`
    end

    def validate_user_details
        return (user_email.length > 0) && (user_name.length > 0)
    end

    #----------------------------------------#

    def user_name
      (ENV['GIT_COMMITTER_NAME'] || github_user_name || `git config user.name` || `<GITHUB_USERNAME>` ).strip
    end

    def github_user_name
      github_user_name = `security find-internet-password -s 192.168.200.71 | grep acct | sed 's/"acct"<blob>="//g' | sed 's/"//g'`.strip
      is_valid = github_user_name.empty? or github_user_name.include? '@'
      return is_valid ? nil : github_user_name
    end

    def local_user_name
      ( `id -F` ).strip
    end

    def user_email
      (ENV['GIT_COMMITTER_EMAIL'] || `git config user.email`).strip
    end

    def year
      Time.now.year.to_s
    end

    def date
      Time.now.strftime "%m/%d/%Y"
    end

    def podfile_path
      'Example/Podfile'
    end

    def podspec_path
      'NAME.podspec'
    end

    #----------------------------------------#
  end
end
